/*
  Repeating Web client

 This sketch connects to a a web server and makes a request
 using a Wiznet Ethernet shield. You can use the Arduino Ethernet shield, or
 the Adafruit Ethernet shield, either one will work, as long as it's got
 a Wiznet Ethernet module on board.

 This example uses DNS, by assigning the Ethernet client with a MAC address,
 IP address, and DNS address.

 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13

 created 19 Apr 2012
 by Tom Igoe
 modified 21 Jan 2014
 by Federico Vanzati

 http://www.arduino.cc/en/Tutorial/WebClientRepeating
 This code is in the public domain.

 */
#include "DHT.h"
#include <SPI.h>
#include <Ethernet.h>

// assign a MAC address for the ethernet controller.
// fill in your address here:
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
// Set the static IP address to use if the DHCP fails to assign
IPAddress ip(192, 168, 100, 110);
IPAddress myDns(192, 168, 0, 1);

#define DHTPIN 7     // what digital pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
DHT dht(DHTPIN, DHTTYPE);

// initialize the library instance:
EthernetClient client;

// char server[] = "192.168.100.111";  // also change the Host line in httpRequest()
const uint16_t port = 8000;
IPAddress server(192,168,100,120);

unsigned long lastConnectionTime = 0;           // last time you connected to the server, in milliseconds
const unsigned long postingInterval = 10*1000;  // delay between updates, in milliseconds / 10 second --> 10 *

void setup() {
  // You can use Ethernet.init(pin) to configure the CS pin
  //Ethernet.init(10);  // Most Arduino shields
  //Ethernet.init(5);   // MKR ETH shield
  //Ethernet.init(0);   // Teensy 2.0
  //Ethernet.init(20);  // Teensy++ 2.0
  //Ethernet.init(15);  // ESP8266 with Adafruit Featherwing Ethernet
  //Ethernet.init(33);  // ESP32 with Adafruit Featherwing Ethernet

  // start serial port:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // start the Ethernet connection:
  Serial.println("Initialize Ethernet with DHCP:");
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // Check for Ethernet hardware present
    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
      while (true) {
        delay(1); // do nothing, no point running without Ethernet hardware
      }
    }
    if (Ethernet.linkStatus() == LinkOFF) {
      Serial.println("Ethernet cable is not connected.");
    }
    // try to congifure using IP address instead of DHCP:
    Ethernet.begin(mac, ip, myDns);
    Serial.print("My IP address: ");
    Serial.println(Ethernet.localIP());
  } else {
    Serial.print("  DHCP assigned IP ");
    Serial.println(Ethernet.localIP());
  }
  // give the Ethernet shield a second to initialize:
  delay(1000);

  Serial.println(F("Begin Setup Suhu"));
  dht.begin();
}

void loop() {
  // if there's incoming data from the net connection.
  // send it out the serial port.  This is for debugging
  // purposes only:
  if (client.available()) {
    char c = client.read();
    Serial.write(c);
  }

  // if ten seconds have passed since your last connection,
  // then connect again and send data:
  if (millis() - lastConnectionTime > postingInterval) {
    httpRequest();
  }

}

// this method makes a HTTP connection to the server:
void httpRequest() {
  // close any connection before send a new request.
  // This will free the socket on the WiFi shield
  client.stop();

  Serial.println("Read Temperatur and Humadity");
  float humadity = dht.readHumidity();   //Read Homadity
  // float humadity = 40;   //Read Homadity
  float tempC = dht.readTemperature(); // Read temperature as Celsius (the default)
  // float tempC = 20; // Read temperature as Celsius (the default)
  float tempF = dht.readTemperature(true); // Read temperature as Fahrenheit (isFahrenheit = true)
  // float tempF = 75; // Read temperature as Fahrenheit (isFahrenheit = true)

  if (isnan(humadity) || isnan(tempC) || isnan(tempF)) {
    Serial.println("Failed to read from DHT sensor!");
    return ;
  }

  // Compute heat index in Fahrenheit (the default)
  // float hiF = 40;
  float hiF = dht.computeHeatIndex(tempF, humadity);
  // Compute heat index in Celsius (isFahreheit = false)
  float hiC = dht.computeHeatIndex(tempC, humadity, false);
  // float hiC = 50;

  Serial.print(F("Humidity: "));
  Serial.print(humadity);
  Serial.print(F("%  Temperature: "));
  Serial.print(tempC);
  Serial.print(F("°C "));
  Serial.print(tempF);
  Serial.print(F("°F  Heat index: "));
  Serial.print(hiC);
  Serial.print(F("°C "));
  Serial.print(hiF);
  Serial.println(F("°F"));

  Serial.print("connecting to : ");
  Serial.println(server);

  // We now create a URI for the request
  String url = "/api/temperatures";
  String data= "humadity=";
  data+=humadity;
  data+="&temp_c=";
  data+=tempC;
  data+="&temp_f=";
  data+=tempF;
  data+="&heat_index_c=";
  data+=hiC;
  data+="&heat_index_f=";
  data+=hiF;
  data+="&location=Lemari ES";
  data+="&ip=";
  data+=DisplayAddress(Ethernet.localIP());

  // if there's a successful connection:
  if (client.connect(server, port)) {
    Serial.println("connecting...");
    // send the HTTP GET request:
    client.println(String("POST "+url+" HTTP/1.1"));
    client.println(String("Host: ")+server);
    // client.println("User-Agent: arduino-ethernet");
    client.println("Cache-Control: no-cache");
    client.println("Content-Type: application/x-www-form-urlencoded");
    client.print("Content-Length: ");
    client.println(data.length());
    client.println();
    client.println(data);
    client.println("Connection: close");
    client.println();

    // note the time that the connection was made:
    lastConnectionTime = millis();
  } else {
    // if you couldn't make a connection:
    Serial.println("connection failed");
  }
}

String DisplayAddress(IPAddress address)
{
 return String(address[0]) + "." + 
        String(address[1]) + "." + 
        String(address[2]) + "." + 
        String(address[3]);
}




